'use strick';

exports.signIn = signIn;
exports.signUp = signUp;
exports.loginRequired = loginRequired;

var userModel = require('../models/user.model');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

function signUp(req, res) {
    var user = new userModel(req.body);
    user.name = req.body.name;
    user.emailId = req.body.emailId;
    user.hash_password = bcrypt.hashSync(req.body.password, 10); //encrypt the password with bcrypt algorithm

    user.save(function (err) {
        if(err)
            res.status(500).send({
               code:500,
               message:"Internal server error : " + err
            });
        res.status(200).send({
           code:200,
           message: "SignUp successfully!",
           data: user
        });
    });
}

function signIn(req, res) {
    var User = new userModel(req.body);
    User.findOne({
        emailId: req.body.emailId
    }, function(err, user) {
        if (err) throw err;
        if (!user) {
            res.status(401).json({ message: 'Authentication failed. User not found.' });
        } else if (user) {
            if (!user.comparePassword(req.body.password)) {
                res.status(401).json({ message: 'Authentication failed. Wrong password.' });
            } else {
                return res.json({
                    code:200,
                    message: "Logging successfully!",
                    data: user,
                    token: jwt.sign({
                            email: user.email,
                            fullName: user.fullName,
                            _id: user._id
                        },
                        'RESTFULAPIs'
                    )
                });
            }
        }
    });
}

function loginRequired(req, res, next) {
    if (req.user) {
        next();
    } else {
        return res.status(401).json({ message: 'Unauthorized user!' });
    }
}