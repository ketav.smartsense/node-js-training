var mongoose = require('mongoose');
var Schema = mongoose.schema;
var bcrypt = require('bcryptjs');

var UserSchema = new Schema({
    name: {
        type: String,
        required:true,
    },
    emailId: {
        type: String,
        unique: true,
        toLocaleLowerCase:true,
        required:true,
    },
    password: {
        type: String,
    },
    status: {
        type: Number,
        default: 1
        //1 = 'Active', 2 = "Inactive"
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
});

UserSchema.method.comparePassword = function (password) {
    return bcrypt.compareSync(password, this.hash_password);
}

module.exports = mongoose.model('User', UserSchema);