var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var db = require('./config/db');
var productCtrl = require('./controller/product.controller');
var userCtrl = require('./controller/user.controller');


var app = express();

app.use(bodyParser.urlencoded({ extended:false }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;

// var router = express.Router();

app.use(cors());
// app.use('/api', router);
app.listen(port);

console.log('REST API is runnning at ' + port);

function middleHandler(req, res, next) {
    console.log("execute middle ware");
    next();
}

//route for products
app.route('/products')
    .post(productCtrl.create) //create new product
    .get(productCtrl.getAllProducts) //select all products

app.route('/products/:productId')
    .get(productCtrl.getProduct) //select perticular record
    .put(productCtrl.updateProduct) //update product
    .delete(productCtrl.deleteProduct) //delete product


//routes for users
app.route('/user')
    .post(userCtrl.signUp)

app.route('/user/login')
    .post(userCtrl.signIn)


