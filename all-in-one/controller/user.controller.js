exports.signIn = signIn;
exports.signUp = signUp;


var userModel = require('./../models/user.model');
var productModel = require('./../models/product.model');
var jwt = require('jsonwebtoken');
var config = require('./../config/config');

function signUp(req, res) {
    var user = new userModel();
    user.firstName = req.body.firstName;
    user.lastName = req.body.lastName;
    user.email = req.body.email;
    user.password = req.body.password;
    user.createdDate = Date.now(),
    user.updateDate = Date.now(),
    user.status = 1; //static value; 1 - Active

    user.save(function (err) {
        if(err)
            res.status(500).send({
                code:500,
                message:"Internal server error : " + err
            });
        res.status(200).send({
            code:200,
            message: "SignUp successfully!",
            data: user
        });
    });
}

function signIn(req, res) {

    userModel.findOne({
        email: req.body.email
    }, function(err, user) {
        if (err)
            res.send({
               code: 500,
               message: "Internal server error : "+ err,
            });
        if (!user) {
            res.status(404).json({ message: 'Authentication failed. User not found.' });
        } else if (user) {
            if (!user.comparePassword(req.body.password)) {
                res.status(404).json({ message: 'Authentication failed. Wrong password.' });
            } else {
                return res.json({
                    code:200,
                    message: "Logging successfully!",
                    data: user,
                    token: jwt.sign ({ _id: user._id },config.superSecret,{ expiresIn: 86400 })
                    //in seconds
                });
            }
        }
    });
}

