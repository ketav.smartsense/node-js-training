exports.create = addProduct;
exports.getAllProducts = getAllProducts;
exports.getProduct = getProduct;
exports.updateProduct = updateProduct;
exports.deleteProduct = deleteProduct;
exports.validateAddProduct = validateAddProduct;
exports.validateGetProduct = validateGetProduct;
exports.validateUpdateProduct = validateUpdateProduct;

var product = require('./../models/product.model');
var auth = require('./../general/auth');
var response = require('./../general/MyResponse');

function addProduct(req, res) {
    var validateUser = auth.validateUser(req, res);
    if(validateUser.status == 200) {
        var p = new product();
        p.title = req.body.title;
        p.price = req.body.price;
        p.instock = req.body.instock;

        p.save(function (err) {
            if (err) {
                res.send({error: "Error occured while create product : " + err});
            }
            res.send({message: "Product created successfully!"});
        });
    } else {
        res.status(validateUser.status).send({code:validateUser.status, message: validateUser.message});
    }
}

function getAllProducts(req, res) {
    var validateUser = auth.validateUser(req, res);
    if(validateUser.status == 200) {
        product.find(function (err, products) {
            if(err)
                response.createResponse(req, res,500,"Error occured while get product : " + err, {}, {})
            response.createResponse(req, res,200,'Products found!', products, {});
        });
    } else {
        response.createResponse(req, res,validateUser.status,validateUser.message, {}, {});
    }
}

function getProduct(req, res) {
    var validateUser = auth.validateUser(req, res);
    if(validateUser.status == 200) {
        product.findById(req.params.productId, function (err, p) {
            if(err)
                response.createResponse(req, res, 500,"Error occured while get product : " + err, {}, {});
            response.createResponse(req, res, 200, "Product found.", p, {});
        });
    } else {
        response.createResponse(req,res,validateUser.status,validateUser.message,{},{});
    }
}

function updateProduct(req, res) {
    var validateUser = auth.validateUser(req, res);
    if(validateUser.status == 200) {
        product.findById(req.params.productId, function (err, prod) {
            if(err)
                response.createResponse(req, res, 404, "Product not found.", {}, {});
            prod.title = req.body.title;
            prod.price = req.body.price;
            prod.instock = req.body.instock;

            prod.save(function (err) {
                if(err)
                    response.createResponse(req, res, 500, "Something went wrong while product update : "+err, {}, {});
                response.createResponse(req, res, 200, "Product updated successfully!", prod, {});
            });
        });
    } else {
        response.createResponse(req, res, validateUser.status, validateUser.message, {}, {});
    }
}

function deleteProduct(req, res) {
    var validateUser = auth.validateUser(req, res);
    if(validateUser.status == 200) {
        product.remove({_id: req.params.productId}, function (err, prod) {
            if(err)
                response.createResponse(req, res, 500, "Internal server error : "+err, {}, {});
            response.createResponse(req, res, 200, "Product deleted successfully.", {}, {});
        });
    } else {
        response.createResponse(req, res, validateUser.status, validateUser.message, {}, {});
    }
}

function validateAddProduct(req, res, next) {
    req.checkBody('title', 'Title is required').notEmpty();
    req.checkBody('price', 'Price is required').notEmpty();
    req.checkBody('price', 'Price must be a numeric').isNumeric();
    req.checkBody('instock', 'InStock is required').notEmpty();
    req.checkBody('instock', 'InStock must be in true / false.').isBoolean();

    var errors = req.validationErrors();
    if (errors) {
        response.createResponse(req, res, 400, errors[0].msg, {}, {});
    }
    return next();
}

function validateGetProduct(req, res, next) {
    req.checkParams('productId', 'Product ID is required').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        response.createResponse(req, res, 400, errors[0].msg, {}, {});
    }
    return next();
}

function validateUpdateProduct(req, res, next) {
    req.checkParams('productId', 'Product ID is required').notEmpty();
    req.checkBody('title', 'Title is required').notEmpty();
    req.checkBody('price', 'Price is required').notEmpty();
    req.checkBody('price', 'Price must be a numeric').isNumeric();
    req.checkBody('instock', 'InStock is required').notEmpty();
    req.checkBody('instock', 'InStock must be in true / false.').isBoolean();

    var errors = req.validationErrors();
    if (errors) {
        response.createResponse(req, res, 400, errors[0].msg, {}, {});
    }
    return next();
}