var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');

var Schema = mongoose.Schema;

var UserSchema = new Schema({
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    email: {
        type: String,
        unique: true
    },
    password: {
        type: String
    },
    status: {
        type: Number,
        default: 1,
        //1-Active, 2-InActive, 9-Delete
    },
    createdDate: {
        type: Date,
        default: Date.now()
    },
    updateDate: {
        type: Date,
        default: Date.now()
    }
});

/**
 * Hook a pre save method to hash the password
 */
UserSchema.pre('save', function (next) {
    encPassword = bcrypt.hashSync(this.password, 10);
    this.password = encPassword;
    next();
});

/**
 * Compare password
 */
UserSchema.methods.comparePassword = function (password) {
    console.log(bcrypt.compareSync(password, this.password));
    return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('userModel', UserSchema);