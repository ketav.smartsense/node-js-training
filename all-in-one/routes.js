var routes = require('express').Router();
var productRoute = require('./routes/product.route');
var userRoute = require('./routes/user.route');

routes.use('/products', productRoute);
routes.use('/user', userRoute);

routes.all('/', function (req, res) {
    res.send("/ here");
});
module.exports = routes;
