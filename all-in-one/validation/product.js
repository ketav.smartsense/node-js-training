var Joi = require('joi');

module.exports = {
    create: {
        title: Joi.string().required()
    }
};