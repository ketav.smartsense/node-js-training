
var config = require('./config/config');
var mongoose = require('mongoose');
var express = require('express');
var bodyParser = require('body-parser');
var validate = require('express-validator');
var routes = require('./routes');

var app = express();
var port = process.env.PORT || 8080;

app.use(bodyParser.urlencoded({ extended:false }));
app.use(bodyParser.json());
app.use(validate());

app.use(routes);
app.listen(port, console.log('REST API is runnning at ' + port));

// create DB connection
mongoose.connect(config.dbConnectionString);

module.exports = app;