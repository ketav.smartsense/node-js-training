var routes = require('express').Router();
var productCtrl = require('./../controller/product.controller');

routes.post('/',productCtrl.validateAddProduct,productCtrl.create); //create new product
routes.get('/',productCtrl.getAllProducts); //select all products

routes.get('/:productId', productCtrl.validateGetProduct, productCtrl.getProduct)
routes.put('/:productId', productCtrl.validateUpdateProduct ,productCtrl.updateProduct)
routes.delete('/:productId', productCtrl.validateGetProduct ,productCtrl.deleteProduct)

module.exports = routes;