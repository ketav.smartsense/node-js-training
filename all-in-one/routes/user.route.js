var routes = require('express').Router();
var userController = require('./../controller/user.controller');

routes.post('/', userController.signUp);
routes.post('/login', userController.signIn);

module.exports = routes;

