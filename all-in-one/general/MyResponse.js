exports.createResponse = createResponse;

// Function to create response
function createResponse(req, res, status, message, data, extra)
{
    return res.status(status).send({
        'status' : status,
        'message' : message,
        'data' : data,
        'extra' : extra
    });
}