exports.validateUser = validateUser;

var jwt = require('jsonwebtoken');
var config = require('./../config/config');

function validateUser(req, res) {
    var response = {};
    if( req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] == 'bearer'){
        jwt.verify(req.headers.authorization.split(' ')[1], config.superSecret, function (err, decode) {
            if(err){
                response = { status:"401", message: "Failed to authenticate token."};
            } else {
                response = {status: "200", data: decode};
            }
        });
    } else {
        response = {status: "404", message: "Invalid auth token"};
    }
    return response;
}
