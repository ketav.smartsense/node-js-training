var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductSchema = new Schema({
    title: String,
    price: Number,
    instock: Boolean
});

module.exports = mongoose.model('Product',ProductSchema);