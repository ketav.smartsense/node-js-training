var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var db = require('./DB/db');
var product = require('./models/product');
//jwt start from here
var jwt = require('jsonwebtoken');

var app = express();

app.use(bodyParser.urlencoded({ extended:true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;

var router = express.Router();

app.use(cors());
app.use('/api', router);
app.listen(port);

console.log('REST API is runnning at ' + port);



router.use(function (req,res, next) {
    console.log('Logging of request will be done here');
    next();
});

//create product (insert)
router.route('/products').post(function (req, res) {
   var p = new product();

   p.title = req.body.title;
   p.price = req.body.price;
   p.instock = req.body.instock;

   p.save(function (err) {
      if(err){
          res.send({error : "Error occured while create product : " + err});
      }
      res.send({ message:"Product created successfully!"});
   });
});

//select records (get data)
router.route('/products').get(function (req, res) {
   product.find(function (err, products) {
      if(err)
          res.send({
              code:500,
              error:"Error occured while get product : " + err
          });
      res.send({
          code:200,
          message: "Products found!",
          data: products
      });
   });
});

//select perticular record
router.route('/products/:productId').get(function (req, res) {
    product.findById(req.params.productId, function (err, p) {
        if(err)
            res.send({
                code:500,
                error:"Error occured while get product : " + err
            });
        res.send({
            code:200,
            message: "Product found.",
            data:p
        });
    });
});

//update data
router.route('/products/:productId').put(function (req, res) {
    console.log(req.body);
    product.findById(req.params.productId, function (err, prod) {
        if(err)
            res.send({
                error: "Product not found."
            });

        prod.title = req.body.title;
        prod.price = req.body.price;
        prod.instock = req.body.instock;

        prod.save(function (err) {
            if(err)
                res.send({
                    error: "Something went wrong while product update : "+err
                });

            res.send({
                code:200,
                message:"Product updated successfully!",
                data: prod
            });
        });
    });
});

//delete  data
router.route('/products/:productId').delete(function (req, res) {
   product.remove({_id: req.params.productId}, function (err, prod) {
       if(err)
           res.send({
              code : 500,
              error: "Internal server error : "+err
           });

       res.send({
          code:200,
          message:"Product deleted successfully."
       });
   });
});