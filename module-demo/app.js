var http = require('http');
var fs = require('fs');
var module1 = require('./module1');


function onRequest(req, res) {
	// res.json({ 'response': 'a GET request for LOOKING at questions' });
	res.writeHead(200,{'Content-Type': 'text/plain'});
	res.write(module1.myVariable);
	module1.myFunction();
	res.end();
}


http.createServer(onRequest).listen(5000);
