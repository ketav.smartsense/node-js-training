var http = require('http');
var formidable = require('formidable');
var fs = require('fs');
var constant = require('./constant');

http.createServer(routes).listen(constant.PORT);


function routes(req, res) {
    console.log(constant.SERVER_CONNECT + constant.PORT);

    if(req.url == '/'){
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write('<form action="fileupload" method="post" enctype="multipart/form-data">');
        res.write('<input type="file" name="filetoupload"><br>');
        res.write('<input type="text" name="firstName" value="test purpose!!"><br>');
        res.write('<input type="submit">');
        res.write('</form>');
        return res.end();
    }
    if(req.method == 'POST' && req.url == '/fileupload'){
        console.log("Form Submited");
        var form = new formidable.IncomingForm();
        // parsing the form
        form.parse(req, function (err, fields, files) {
            var oldpath = files.filetoupload.path;
            // return "Done";
            var newpath = '/var/www/node-js-training/file-upload/uploads/' + files.filetoupload.name;
            fs.rename(oldpath, newpath, function (err) {
                if (err) throw err;
                res.write('File uploaded and moved!');
                res.end();
            });
        });
    } else{
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write('Invalid URL.');
        return res.end();
    }
}